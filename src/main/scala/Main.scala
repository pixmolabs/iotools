import io.eels.component.json.JsonSink
import io.eels.component.parquet.ParquetSource
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}

import scala.io.Source

object IOHdfs {
  def writeToHdfs(s: String, path: Path)(fs: FileSystem) = {
    val os = fs.create(path)
    os.write(s.getBytes)
    os.close()
  }

  def readFromHdfs(path: Path)(fileSystem: FileSystem) = {
    val is = fileSystem.open(path)
    val s = Source.fromInputStream(is).getLines().mkString("\n")
    is.close()
    s
  }
}


object Main extends App {
  println(" --- IOTools --- ")

  // Configufation HDFS
  val configuration = new Configuration()
  configuration.set("fs.defaultFS", "hdfs://localhost:9000")

  // Get FS
  val hdfs = FileSystem.get(configuration)

  // Write a file to Hdfs
  IOHdfs.writeToHdfs("France;Angleterre;Italie", new Path("/ref/country.csv"))(hdfs)

  // Read a file from Hdfs
  println(IOHdfs.readFromHdfs(new Path("/ref/country.csv"))(hdfs))

  //Convert a parquet file to Json
  val source = ParquetSource(new Path("/ref/data.parquet"))(hdfs, configuration)
  val sink = JsonSink(new Path("/ref/data.json"))(hdfs)

  source.toDataStream().to(sink)

  println(IOHdfs.readFromHdfs(new Path("/ref/data.json"))(hdfs))

  hdfs.close()
  println(" --- IOTools --- ")
}
